package online.devliving.recyclerviewsample;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import online.devliving.recyclerviewsample.recyclerview.RecyclableViewItem;
import online.devliving.recyclerviewsample.recyclerview.RecyclerViewAdapter;
import online.devliving.recyclerviewsample.recyclerview.items.HeadlineGridItem;
import online.devliving.recyclerviewsample.recyclerview.items.HeadlineListItem;
import online.devliving.recyclerviewsample.recyclerview.items.SourceHeaderItem;
import online.devliving.recyclerviewsample.util.MultiMap;
import online.devliving.recyclerviewsample.webservices.NewsApiManager;

public class MainActivity extends RxAppCompatActivity {

    RecyclerView mList;
    ContentLoadingProgressBar mProgress;
    NewsApiManager mApiManager;

    RecyclerViewAdapter mAdapter;
    Disposable mApiCall;

    boolean showList = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fresco.initialize(getApplicationContext());

        mList = findViewById(R.id.list_view);
        mProgress = findViewById(R.id.progress_bar);

        mApiManager = new NewsApiManager();
    }

    @Override
    protected void onResume() {
        super.onResume();

        reloadNews();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(mApiCall != null && !mApiCall.isDisposed()) {
            mApiCall.dispose();
            mProgress.hide();
        }
    }

    void reloadNews() {
        mProgress.show();
        MultiMap<String, RecyclableViewItem> map = new MultiMap<>();

        mApiCall = mApiManager.getHeadlines(Locale.getDefault().getCountry().toLowerCase())
                .map(newsHeadline -> {
                    RecyclableViewItem item = showList? new HeadlineListItem(newsHeadline):new HeadlineGridItem(newsHeadline);
                    map.put(showList ?  newsHeadline.getSource().getName() : "all", item);

                    return 0;
                })
                .count()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .compose(bindToLifecycle())
        .subscribe(count -> {
            List<RecyclableViewItem> items = new ArrayList<>();

            if(showList) {
                for(String key : map.getKeySet()){
                    items.add(new SourceHeaderItem(key));
                    items.addAll(map.get(key));
                }
            } else {
                items = map.get("all");
            }

            updateList(items);
        }, error -> {
            Log.e("MAIN", "Error loading news", error);
            mProgress.hide();
        });
    }

    void updateList(List<RecyclableViewItem> items) {
        mAdapter = new RecyclerViewAdapter(items);
        mList.setLayoutManager(showList? new LinearLayoutManager(this):new GridLayoutManager(this, 2));
        mList.setHasFixedSize(true);
        mList.setAdapter(mAdapter);

        mProgress.hide();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.toggle_view) {
            showList = !showList;
            invalidateOptionsMenu();
            reloadNews();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.toggle_view);
        item.setIcon(showList? R.drawable.ic_view_module_black_24dp:R.drawable.ic_view_list_black_24dp);

        Drawable icon = DrawableCompat.wrap(item.getIcon());
        DrawableCompat.setTint(icon, ContextCompat.getColor(this, R.color.colorAccent));
        item.setIcon(icon);
        return true;
    }
}
