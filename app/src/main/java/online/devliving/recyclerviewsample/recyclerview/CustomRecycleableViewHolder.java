package online.devliving.recyclerviewsample.recyclerview;

/**
 * Created by Mehedi Hasan Khan <mehedi.mailing@gmail.com> on 5/11/18.
 */
public interface CustomRecycleableViewHolder {
    /**
     * Good place to get rid of heavy resources
     */
    void onViewRecycled();

    /**
     * Time to clear animations, make it non-transient
     * @return
     */
    boolean onFailedToRecycleView();

    /**
     * good place to allocate, setup resources
     */
    void onViewAttachedToWindow();

    /**
     * good place get rid of resources
     */
    void onViewDetachedFromWindow();
}
