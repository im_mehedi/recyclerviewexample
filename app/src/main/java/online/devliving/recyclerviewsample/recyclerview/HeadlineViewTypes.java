package online.devliving.recyclerviewsample.recyclerview;

/**
 * Created by Mehedi Hasan Khan <mehedi.mailing@gmail.com> on 5/13/18.
 */
public enum HeadlineViewTypes {
    VIEW_TYPE_LIST,
    VIEW_TYPE_GRID,
    VIEW_TYPE_HEADER;
}
