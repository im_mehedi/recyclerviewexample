package online.devliving.recyclerviewsample.recyclerview;

/**
 * Created by Mehedi on 6/2/15.
 *
 * An item that is uniquely identifiable
 */
public interface Identifiable {
    /**
     *
     * @return The unique ID of this item
     */
    int getId();
}
