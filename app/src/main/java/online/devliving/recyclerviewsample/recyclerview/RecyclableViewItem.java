package online.devliving.recyclerviewsample.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by Mehedi.
 *
 * An item that is Recyclable, can be shown in a RecyclerView
 */
public interface RecyclableViewItem<T extends RecyclerView.ViewHolder> {
    /**
     *
     * @return Type of View
     */
    int getViewType();

    /**
     *
     * @param parent
     * @param itemClickListener
     * @return Create and return the ViewHolder
     */
    T onCreateViewHolder(ViewGroup parent, RecyclerViewAdapter.OnItemClickListener itemClickListener);

    /**
     * Update the View
     * @param viewHolder
     */
    void onBindViewHolder(RecyclerView.Adapter adapter, T viewHolder);
}
