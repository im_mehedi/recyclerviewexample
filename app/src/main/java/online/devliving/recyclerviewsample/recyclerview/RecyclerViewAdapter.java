package online.devliving.recyclerviewsample.recyclerview;

/**
 * Created by Mehedi Hasan Khan <mehedi.mailing@gmail.com> on 5/11/18.
 */

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mehedi on 5/31/15.
 *
 * A Generic Adapter for RecyclerView
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /**
     * Stores the items
     */
    private List<RecyclableViewItem> mDataStore;

    // callback listener for click events
    protected OnItemClickListener mItemClickListener;

    /**
     * Maps the type of item with the position of such an item
     */
    private SparseArray<Integer> mViewTypeMap;


    /**
     *
     * @param data list of items
     */
    public RecyclerViewAdapter(List<RecyclableViewItem> data){
        super();

        mDataStore = data;
        generateViewTypeMapping();
    }

    /**
     *
     * @param data list of items
     * @param itemClickListener listener for item click events
     */
    public RecyclerViewAdapter(List<RecyclableViewItem> data, OnItemClickListener itemClickListener)
    {
        this(data);
        mItemClickListener = itemClickListener;
    }

    /**
     * Generate the mapping for all types of item
     */
    private void generateViewTypeMapping(){
        mViewTypeMap = new SparseArray<Integer>();
        if(mDataStore != null && !mDataStore.isEmpty())
        {
            int position = 0;

            for(RecyclableViewItem item : mDataStore)
            {
                addMappingForItem(item, position);

                position++;
            }
        }
    }

    /**
     *
     * @param item list item
     * @param position position of the item
     */
    private void addMappingForItem(RecyclableViewItem item, int position)
    {
        if(mViewTypeMap.indexOfKey(item.getViewType()) < 0)
        {
            mViewTypeMap.put(item.getViewType(), position);
        }
    }

    /**
     * Add an item at the end/bottom
     * @param item
     */
    public <I extends RecyclableViewItem> void appendItem(I item){
        if(mDataStore == null)
        {
            mDataStore = new ArrayList<>();
        }

        int index = mDataStore.size();
        mDataStore.add(item);

        addMappingForItem(item, index);
        notifyItemInserted(index);
    }

    /**
     * Add an item at a given position
     * @param item
     * @param position
     */
    public <I extends RecyclableViewItem> void addItemAtPosition(I item, int position)
    {
        if(mDataStore == null)
        {
            appendItem(item);
            return;
        }

        mDataStore.add(position, item);
        generateViewTypeMapping();
        notifyItemInserted(position);
    }

    /**
     * Remove the item at given position
     * @param position
     * @return the item that was removed
     */
    public <I extends RecyclableViewItem> I removeItemAtPosition(int position)
    {
        I item = null;

        if(mDataStore != null && mDataStore.size() > position)
        {
            item = (I) mDataStore.remove(position);
            if(mViewTypeMap.get(item.getViewType()) == position)
            {
                generateViewTypeMapping();
            }
            notifyItemRemoved(position);
        }
        return item;
    }

    /**
     * Remove an item
     * @param item
     */
    public <I extends RecyclableViewItem> void removeItem(I item){
        if(mDataStore != null && !mDataStore.isEmpty()){
            int position = mDataStore.indexOf(item);
            if(mDataStore.remove(item))
            {
                if(mViewTypeMap.get(item.getViewType()) == position)
                {
                    generateViewTypeMapping();
                }
                notifyItemRemoved(position);
            }
        }
    }

    /**
     * Set the given item at specified position
     * @param position
     * @param item
     */
    public <I extends RecyclableViewItem> void setItemAtPosition(int position, I item){
        if(mDataStore != null && !mDataStore.isEmpty())
        {
            int viewType = mDataStore.get(position).getViewType();
            mDataStore.set(position, item);
            if(mViewTypeMap.get(viewType) == position)
            {
                generateViewTypeMapping();
            }
            else {
                addMappingForItem(item, position);
            }
            notifyItemChanged(position);
        }
    }

    /**
     *
     * @param position
     * @return item at {@param position}
     */
    public <I extends RecyclableViewItem> I getItemAtPosition(int position)
    {
        if(mDataStore != null && !mDataStore.isEmpty() && position >=0 && position < mDataStore.size())
        {
            return (I) mDataStore.get(position);
        }

        return null;
    }

    /**
     *
     * @param position
     * @return Unique identifier for the item at {@param position}
     */
    @Override
    public long getItemId(int position) {
        if(mDataStore != null && !mDataStore.isEmpty() && position >= 0 && position < mDataStore.size())
        {
            RecyclableViewItem item = mDataStore.get(position);

            if(item instanceof Identifiable)
            {
                return ((Identifiable) item).getId();
            }
        }
        return super.getItemId(position);
    }

    /**
     * Move an item from position {@param fromPos} to position {@param toPos}
     * @param fromPos
     * @param toPos
     */
    public void moveItem(int fromPos, int toPos)
    {
        if(mDataStore != null && !mDataStore.isEmpty())
        {
            RecyclableViewItem item = mDataStore.remove(fromPos);
            mDataStore.add(toPos, item);
            generateViewTypeMapping();

            notifyItemMoved(fromPos, toPos);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        int position = mViewTypeMap.get(viewType);
        return mDataStore.get(position).onCreateViewHolder(parent, mItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        mDataStore.get(position).onBindViewHolder(this, holder);
    }

    @Override
    public int getItemCount() {
        if(mDataStore != null)
        {
            return mDataStore.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if(mDataStore != null)
        {
            return  mDataStore.get(position).getViewType();
        }
        return super.getItemViewType(position);
    }

    @Override
    public void onViewRecycled(@NonNull RecyclerView.ViewHolder holder) {
        if(holder instanceof CustomRecycleableViewHolder){
            ((CustomRecycleableViewHolder) holder).onViewRecycled();
            return;
        }
        super.onViewRecycled(holder);
    }

    @Override
    public boolean onFailedToRecycleView(@NonNull RecyclerView.ViewHolder holder) {
        if(holder instanceof CustomRecycleableViewHolder){
            return ((CustomRecycleableViewHolder) holder).onFailedToRecycleView();
        }
        return super.onFailedToRecycleView(holder);
    }

    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        if(holder instanceof CustomRecycleableViewHolder) {
            ((CustomRecycleableViewHolder) holder).onViewAttachedToWindow();
        }
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        if(holder instanceof CustomRecycleableViewHolder){
            ((CustomRecycleableViewHolder) holder).onViewDetachedFromWindow();
        }
        super.onViewDetachedFromWindow(holder);
    }

    /**
     * interface to listen for item clicks
     */
    public interface OnItemClickListener {
        void onItemClick(int position, View clickedView);
    }
}
