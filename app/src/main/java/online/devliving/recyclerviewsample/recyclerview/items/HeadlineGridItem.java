package online.devliving.recyclerviewsample.recyclerview.items;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import online.devliving.recyclerviewsample.R;
import online.devliving.recyclerviewsample.recyclerview.HeadlineViewTypes;
import online.devliving.recyclerviewsample.recyclerview.RecyclableViewItem;
import online.devliving.recyclerviewsample.recyclerview.RecyclerViewAdapter;
import online.devliving.recyclerviewsample.webservices.response.NewsHeadline;

/**
 * Created by Mehedi Hasan Khan <mehedi.mailing@gmail.com> on 5/14/18.
 */
public class HeadlineGridItem implements RecyclableViewItem<HeadlineGridItem.HeadlineGridItemViewHolder> {

    NewsHeadline headline;

    public HeadlineGridItem(NewsHeadline headline) {
        this.headline = headline;
    }

    /**
     * @return Type of View
     */
    @Override
    public int getViewType() {
        return HeadlineViewTypes.VIEW_TYPE_GRID.ordinal();
    }

    /**
     * @param parent
     * @param itemClickListener
     * @return Create and return the ViewHolder
     */
    @Override
    public HeadlineGridItemViewHolder onCreateViewHolder(ViewGroup parent, RecyclerViewAdapter.OnItemClickListener itemClickListener) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_headline_grid_item, parent, false);
        return new HeadlineGridItemViewHolder(itemView);
    }

    /**
     * Update the View
     *
     * @param adapter
     * @param viewHolder
     */
    @Override
    public void onBindViewHolder(RecyclerView.Adapter adapter, HeadlineGridItemViewHolder viewHolder) {
        viewHolder.updateView(headline);
    }

    class HeadlineGridItemViewHolder extends RecyclerView.ViewHolder {
        SimpleDraweeView newsImage;
        TextView title, author, date;

        DateFormat dateFormatter;

        public HeadlineGridItemViewHolder(View itemView) {
            super(itemView);

            newsImage = itemView.findViewById(R.id.news_image);
            title = itemView.findViewById(R.id.headline_title);
            author = itemView.findViewById(R.id.headline_author);
            date = itemView.findViewById(R.id.headline_date);

            dateFormatter = new SimpleDateFormat("MMM d, yy");
        }

        void updateView(NewsHeadline news) {
            newsImage.setImageURI(news.getUrlToImage());
            title.setText(news.getTitle());
            author.setText(news.getAuthor() + "; " + news.getSource().getName());
            date.setText(dateFormatter.format(news.getPublishedAt()));
        }
    }
}
