package online.devliving.recyclerviewsample.recyclerview.items;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import online.devliving.recyclerviewsample.R;
import online.devliving.recyclerviewsample.recyclerview.HeadlineViewTypes;
import online.devliving.recyclerviewsample.recyclerview.RecyclableViewItem;
import online.devliving.recyclerviewsample.recyclerview.RecyclerViewAdapter;
import online.devliving.recyclerviewsample.webservices.response.NewsHeadline;

/**
 * Created by Mehedi Hasan Khan <mehedi.mailing@gmail.com> on 5/13/18.
 */
public class HeadlineListItem implements RecyclableViewItem<HeadlineListItem.HeadlineListItemViewHolder> {

    NewsHeadline headline;

    public HeadlineListItem(NewsHeadline headline) {
        this.headline = headline;
    }

    /**
     * @return Type of View
     */
    @Override
    public int getViewType() {
        return HeadlineViewTypes.VIEW_TYPE_LIST.ordinal();
    }

    /**
     * @param parent
     * @param itemClickListener
     * @return Create and return the ViewHolder
     */
    @Override
    public HeadlineListItemViewHolder onCreateViewHolder(ViewGroup parent, RecyclerViewAdapter.OnItemClickListener itemClickListener) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.news_headline_list_item, parent, false);
        return new HeadlineListItemViewHolder(itemView);
    }

    /**
     * Update the View
     *
     * @param adapter
     * @param viewHolder
     */
    @Override
    public void onBindViewHolder(RecyclerView.Adapter adapter, HeadlineListItemViewHolder viewHolder) {
        viewHolder.updateView(headline);
    }

    class HeadlineListItemViewHolder extends RecyclerView.ViewHolder {
        TextView title, date, author, details;
        DateFormat dateFormatter;

        public HeadlineListItemViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.headline_title);
            date = itemView.findViewById(R.id.headline_date);
            author = itemView.findViewById(R.id.headline_author);
            details = itemView.findViewById(R.id.headline_news);

            dateFormatter = new SimpleDateFormat("EEE, MMM d, yy");
        }

        void updateView(NewsHeadline data){
            title.setText(data.getTitle());
            date.setText(dateFormatter.format(data.getPublishedAt()));
            author.setText(data.getAuthor() + "; " + data.getSource().getName());
            details.setText(data.getDescription());
        }
    }
}
