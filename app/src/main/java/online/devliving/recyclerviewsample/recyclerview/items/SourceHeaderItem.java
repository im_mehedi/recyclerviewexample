package online.devliving.recyclerviewsample.recyclerview.items;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import online.devliving.recyclerviewsample.R;
import online.devliving.recyclerviewsample.recyclerview.HeadlineViewTypes;
import online.devliving.recyclerviewsample.recyclerview.RecyclableViewItem;
import online.devliving.recyclerviewsample.recyclerview.RecyclerViewAdapter;

/**
 * Created by Mehedi Hasan Khan <mehedi.mailing@gmail.com> on 6/27/18.
 */
public class SourceHeaderItem implements RecyclableViewItem<SourceHeaderItem.SourceHeaderItemViewHolder>{

    String sourceName;

    public SourceHeaderItem(String sourceName) {
        this.sourceName = sourceName;
    }

    /**
     * @return Type of View
     */
    @Override
    public int getViewType() {
        return HeadlineViewTypes.VIEW_TYPE_HEADER.ordinal();
    }

    /**
     * @param parent
     * @param itemClickListener
     * @return Create and return the ViewHolder
     */
    @Override
    public SourceHeaderItemViewHolder onCreateViewHolder(ViewGroup parent, RecyclerViewAdapter.OnItemClickListener itemClickListener) {
        Context context = parent.getContext();
        FrameLayout layout = new FrameLayout(context);
        layout.setPadding(8, 8, 8, 8);
        layout.setBackgroundColor(Color.DKGRAY);

        TextView header = new TextView(context);
        header.setId(R.id.headline_title);
        header.setPadding(8, 8, 8, 8);
        header.setTextColor(context.getResources().getColor(R.color.text_accent));
        header.setTextSize(18);

        ViewGroup.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layout.setLayoutParams(params);

        layout.addView(header, params);

        return new SourceHeaderItemViewHolder(layout);
    }

    /**
     * Update the View
     *
     * @param adapter
     * @param viewHolder
     */
    @Override
    public void onBindViewHolder(RecyclerView.Adapter adapter, SourceHeaderItemViewHolder viewHolder) {
        viewHolder.header.setText(sourceName);
    }

    class SourceHeaderItemViewHolder extends RecyclerView.ViewHolder {
        TextView header;

        public SourceHeaderItemViewHolder(View itemView) {
            super(itemView);
            header = itemView.findViewById(R.id.headline_title);
        }
    }
 }
