package online.devliving.recyclerviewsample.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MultiMap<T,F> {

	Map<T,List<F>> Collection;
	public MultiMap()
	{
		Collection=new LinkedHashMap<T, List<F>>();
	}
	
	public void put(T t,F val)
	{
		if(Collection.containsKey(t))
		{
			ArrayList<F> list=(ArrayList<F>) Collection.get(t);
			list.add(val);
		}
		else
		{
			ArrayList<F> list=new ArrayList<F>();
			list.add(val);
			Collection.put(t, list);
		}
	}
	
	public ArrayList<F> get(T key)
	{
		return new ArrayList<F>(Collection.get(key));
	}
	
	public Set<T> getKeySet()
	{
		return Collection.keySet();
	}
}
