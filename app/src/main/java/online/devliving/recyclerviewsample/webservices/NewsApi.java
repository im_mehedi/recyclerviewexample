package online.devliving.recyclerviewsample.webservices;

import io.reactivex.Single;
import online.devliving.recyclerviewsample.webservices.response.NewsHeadlineResponse;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by Mehedi Hasan Khan <mehedi.mailing@gmail.com> on 5/11/18.
 */
public interface NewsApi {

    String API_Key = ""; //TODO: fill this, visit: https://newsapi.org/

    @Headers("X-Api-Key: " + API_Key)
    @GET("top-headlines/")
    Single<NewsHeadlineResponse> getHeadlines(@Query("country") String countryCode);
}
