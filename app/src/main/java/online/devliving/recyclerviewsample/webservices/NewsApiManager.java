package online.devliving.recyclerviewsample.webservices;

import io.reactivex.Observable;
import online.devliving.recyclerviewsample.webservices.response.NewsHeadline;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mehedi Hasan Khan <mehedi.mailing@gmail.com> on 5/11/18.
 *
 * Can and should be used as a Singleton
 */

public class NewsApiManager {

    private NewsApi newsApi;

    public NewsApiManager() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://newsapi.org/v2/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        newsApi = retrofit.create(NewsApi.class);
    }

    public Observable<NewsHeadline> getHeadlines(String countryCode) {
        return newsApi.getHeadlines(countryCode)
                .flatMapObservable(response -> Observable.fromArray(response.getArticles()));
    }
}
