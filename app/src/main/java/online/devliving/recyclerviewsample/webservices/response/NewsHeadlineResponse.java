package online.devliving.recyclerviewsample.webservices.response;

/**
 * Created by Mehedi Hasan Khan <mehedi.mailing@gmail.com> on 5/11/18.
 */
public class NewsHeadlineResponse {
    String status;
    int totalResults;
    NewsHeadline[] articles;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public NewsHeadline[] getArticles() {
        return articles;
    }

    public void setArticles(NewsHeadline[] articles) {
        this.articles = articles;
    }
}


